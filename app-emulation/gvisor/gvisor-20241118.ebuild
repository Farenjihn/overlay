EAPI=7

inherit go-module

DESCRIPTION="Container Runtime Sandbox"
HOMEPAGE="https://gvisor.dev"

REV="0"

go-module_set_globals

COMMIT="2f1ef2228e0ca861ced3ecc562f2629d6b2aa45a"
SRC_URI="https://github.com/google/${PN}/archive/${COMMIT}.tar.gz -> ${P}-${PV}.${REV}.tar.gz
         https://github.com/farenjihn/${PN}-vendor/releases/download/${PV}/gvisor-${PV}.${REV}-deps.tar.xz"

IUSE="+containerd"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64"

BDEPEND="dev-lang/go"
RDEPEND=""

S="${WORKDIR}/${PN}-${COMMIT}"

PATCHES=(
    "${FILESDIR}/fix-deps.patch"
)

src_compile() {
    sed -i "s/VERSION_MISSING/${PV}.${REV}/" runsc/version/version.go

    mkdir bin
    export GOBIN="${S}/bin"

    # `runsc` needs to be purely statically linked
    # because it's placed in its own sandbox and
    # will not find a libc or any other dynamic
    # dependency
    GOFLAGS="" CGO_ENABLED=0 go install -mod=readonly ./runsc/...

    # the containerd shim is now built from this project
    if use containerd; then
        go install -mod=readonly ./shim/...
    fi
}

src_install() {
    dobin bin/runsc

    if use containerd; then
        newbin bin/shim containerd-shim-runsc-v1
    fi
}
