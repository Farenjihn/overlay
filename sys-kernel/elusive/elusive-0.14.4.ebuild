EAPI=8

CRATES="
    adler2-2.0.0
    aho-corasick-1.1.3
    anstream-0.6.18
    anstyle-1.0.10
    anstyle-parse-0.2.6
    anstyle-query-1.1.2
    anstyle-wincon-3.0.6
    anyhow-1.0.93
    bindgen-0.70.1
    bitflags-2.6.0
    block-buffer-0.10.4
    cc-1.2.1
    cexpr-0.6.0
    cfg-if-1.0.0
    clang-sys-1.8.1
    clap-4.5.21
    clap_builder-4.5.21
    clap_derive-4.5.18
    clap_lex-0.7.3
    colorchoice-1.0.3
    cpufeatures-0.2.15
    crc32fast-1.4.2
    crypto-common-0.1.6
    digest-0.10.7
    either-1.13.0
    env_filter-0.1.2
    env_logger-0.11.5
    equivalent-1.0.1
    flate2-1.0.35
    generic-array-0.14.7
    glob-0.3.1
    hashbrown-0.15.1
    heck-0.5.0
    hermit-abi-0.3.9
    humantime-2.1.0
    indexmap-2.6.0
    is_terminal_polyfill-1.70.1
    itertools-0.13.0
    itoa-1.0.11
    jobserver-0.1.32
    libc-0.2.164
    log-0.4.22
    memchr-2.7.4
    minimal-lexical-0.2.1
    miniz_oxide-0.8.0
    nom-7.1.3
    num_cpus-1.16.0
    object-0.36.5
    once_cell-1.20.2
    pest-2.7.14
    pest_derive-2.7.14
    pest_generator-2.7.14
    pest_meta-2.7.14
    pkg-config-0.3.31
    proc-macro2-1.0.89
    quote-1.0.37
    regex-1.11.1
    regex-automata-0.4.9
    regex-syntax-0.8.5
    rustc-hash-1.1.0
    ryu-1.0.18
    same-file-1.0.6
    serde-1.0.215
    serde_derive-1.0.215
    serde_yaml-0.9.34+deprecated
    sha2-0.10.8
    shlex-1.3.0
    strsim-0.11.1
    syn-2.0.87
    thiserror-1.0.69
    thiserror-2.0.3
    thiserror-impl-1.0.69
    thiserror-impl-2.0.3
    typenum-1.17.0
    ucd-trie-0.1.7
    unicode-ident-1.0.13
    unsafe-libyaml-0.2.11
    utf8parse-0.2.2
    version_check-0.9.5
    walkdir-2.5.0
    winapi-util-0.1.9
    windows-sys-0.59.0
    windows-targets-0.52.6
    windows_aarch64_gnullvm-0.52.6
    windows_aarch64_msvc-0.52.6
    windows_i686_gnu-0.52.6
    windows_i686_gnullvm-0.52.6
    windows_i686_msvc-0.52.6
    windows_x86_64_gnu-0.52.6
    windows_x86_64_gnullvm-0.52.6
    windows_x86_64_msvc-0.52.6
    zstd-0.13.2
    zstd-safe-7.2.1
    zstd-sys-2.0.13+zstd.1.5.6
"

inherit cargo

DESCRIPTION="A custom initramfs generator"
HOMEPAGE="https://github.com/farenjihn/elusive"
SRC_URI="https://github.com/farenjihn/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz
$(cargo_crate_uris)"

LICENSE="0BSD Apache-2.0 BSD Boost-1.0 GPL-3 MIT Unicode-DFS-2016 Unlicense ZLIB"
SLOT="0"
KEYWORDS="amd64"

DEPEND=""

RDEPEND="llvm-core/clang
         sys-apps/kmod"

BDEPEND="${RDEPEND}
        >=dev-lang/rust-1.81"

# rust does not use *FLAGS from make.conf, silence portage warning
# update with proper path to binaries this crate installs, omit leading /
QA_FLAGS_IGNORED="usr/bin/${PN}"

src_compile() {
    cd elusive
    cargo_src_compile
}

src_install() {
    cd elusive

    cargo_src_install --path 'crates/elusive'

    find contrib/config/elusive.d/ -type f -exec sed -i 's#contrib#/usr/share/elusive#g' {} +

    insinto /usr/share/elusive/
    doins -r contrib/config/elusive.d
    doins -r contrib/files

    fperms 0755 /usr/share/elusive/files/init
}
